import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { BillCreate } from '../bill-create/bill-create'



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',

})
export class HomePage {
  billList: FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController, db: AngularFireDatabase, public alertCtrl: AlertController) {
    this.billList = db.list('/bills');

  }

  newBill() {
    this.navCtrl.push(BillCreate);
  }

  promptPayment(billId: string) {
    let alert = this.alertCtrl.create({
      message: "Mark as paid?",
      buttons: [
        {
          text: 'Cancel',
        },
        {
          text: 'Mark as Paid',
          handler: data => {
            this.billList.update(billId, { paid: true });
          }
        }
      ]
    });
    alert.present();
  }
}