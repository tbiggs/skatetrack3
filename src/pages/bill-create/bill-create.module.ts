import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillCreate } from './bill-create';

@NgModule({
  declarations: [
    BillCreate,
  ],
  imports: [
    IonicPageModule.forChild(BillCreate),
  ],
  exports: [
    BillCreate
  ]
})
export class BillCreateModule {}
