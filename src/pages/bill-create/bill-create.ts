import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

/**
 * Generated class for the BillCreate page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bill-create',
  templateUrl: 'bill-create.html',
})
export class BillCreate {
  billList: FirebaseListObservable<any>;


  constructor(public navCtrl: NavController, public navParams: NavParams, db: AngularFireDatabase) {
    this.billList = db.list('/bills');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BillCreate');
  }


  createBill(name, amount, dueDate) {
    this.billList.push({
      name: name, amount: amount, dueDate: dueDate, paid: false
    }).then(
      newBill => {
        this.navCtrl.pop();
      }, error => { console.log(error); });
  }


}
