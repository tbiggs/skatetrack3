import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {MomentModule} from 'angular2-moment';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {BillCreate} from '../pages/bill-create/bill-create'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



export const firebaseConfig = {
    apiKey: "AIzaSyBSFjEMry-zsnDfnUZ0yRYAn5uJAl7w44M",
    authDomain: "skatetrack-a02da.firebaseapp.com",
    databaseURL: "https://skatetrack-a02da.firebaseio.com",
    projectId: "skatetrack-a02da",
    storageBucket: "skatetrack-a02da.appspot.com",
    messagingSenderId: "1012310878395"
  };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    BillCreate
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    BillCreate
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
